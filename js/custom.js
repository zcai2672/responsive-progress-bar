var progressBar = (function () {
    var ngApp = angular.module('ngApp', []);
    ngApp.controller('showProgressBar', function ($scope, $http) {

        $http.get("js/data.json").success(function (config) {
            $scope.data = config;
            $(function () {
                $("#dialog").dialog();
                $("#progressBar").show();
                $('#progressBar').css({border:'solid 1px #dddddd'}) // make sure it log
                $("#progress")
                    .animate({
                        width: '200px'
                    },
                    {
                        duration: config.data.lightbox.duration,
                        step: function (now) {
                            var pct = Math.round((now / 2));
                            $("#progressText").html('<span>Progress ' + pct + '%</span>');

                        },
                        complete: function () {
                            console.log('it is done');
                            $('#progress').css({ 'background': 'url("images/complete.png")'})
                            $("#progressText").html('<span>This task is 100% completed</span>&nbsp;&nbsp;<img src="images/tick.png">');
                        }
                    });

            });

        }).error(function (data, status, headers, config) {
            alert("error!");
        });
    });
})();

function adjustWidget() {
    var yPos = $(window).height() / 2 - $('.ui-dialog').height() / 2;
    var xPos = $(window).width() / 2 - $('.ui-dialog').width() / 2;
    $('.ui-dialog').css({'top': yPos + 'px', 'left': xPos + 'px'});

}
function resetPage(){
    window.location.reload()
}